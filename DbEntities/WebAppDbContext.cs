﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportAndExportExcelDemo.DbEntities
{
    public class WebAppDbContext : DbContext
    {
        public readonly string ConnectionString;

        public DbSet<Article> Articles { get; set; }
        public DbSet<Payroll> Payrolls { get; set; }

        public WebAppDbContext(string connectionString) :
            base()
        {
            ConnectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Article>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Article");
            });

            modelBuilder.Entity<Payroll>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Payroll");
            });

        }
    }
}
