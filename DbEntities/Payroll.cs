﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportAndExportExcelDemo.DbEntities
{
    public class Payroll
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public double Salary { get; set; }

    }
}
