﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportAndExportExcelDemo.DbEntities
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string PersonInCharge { get; set; }

    }
}
