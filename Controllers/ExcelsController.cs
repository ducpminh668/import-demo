﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ImportAndExportExcelDemo.DbEntities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace ImportAndExportExcelDemo.Controllers
{
    [Route("api/excels")]
    [ApiController]
    public class ExcelsController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        private List<Article>  articles = new List<Article>();

        public ExcelsController(IHostingEnvironment hostingEnvironment)
        {
            this._hostingEnvironment = hostingEnvironment;
        }

//        [HttpGet("ImportArticle")]
        public List<Article> Import()
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = "Article.xlsx";

            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            try
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    StringBuilder sb = new StringBuilder();
                    ExcelWorksheets worksheets = package.Workbook.Worksheets;
                    for (int i = 1; i < worksheets.Count+1; i++)
                    {
                        ExcelWorksheet excelWorksheet = package.Workbook.Worksheets[i];
                        AddArticles(excelWorksheet);
                    }
                    return articles;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Payroll> CalculationOfWages()
        {
            var articles = Import();

            if(!articles.Any())
            {
                return null;
            }

            var payrolls = new List<Payroll>();
            foreach (var article in articles)
            {
                double Salary = 0;
                int i = 1;
                switch (article.Id)
                {
                    case 1:
                        Salary = 100 * 0.3;
                        break;
                    case 2:
                        Salary = 100 * 0.25;
                        break;
                    case 3:
                        Salary = 100 * 0.4;
                        break;
                    case 4:
                        Salary = 100 * 0.5;
                        break;
                    case 5:
                        Salary = 100 * 0.35;
                        break;
                    case 6:
                        Salary = 100 * 0.6;
                        break;
                    default:
                        Salary = 100 * 0.1;
                        break;
                }
                string[] listName = article.PersonInCharge.Split(',');

                if(listName.Any())
                {
                    foreach (var item in listName)
                    {
                        var payroll = new Payroll();
                        payroll.Id = i;
                        payroll.EmployeeName = item;
                        payroll.Salary = Salary;

                        i++;
                        payrolls.Add(payroll);
                    }
                }
            }

            return payrolls;
        }

        [HttpGet("Export")]
        public string Export()
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"Payroll.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }

            var comlumHeadrs = new string[]
            {
                "No",
                "EmployeeName",
                "Salary",
            };

            using (ExcelPackage package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Payroll");

                using (var cells = worksheet.Cells[1, 1, 1, 3]) //(1,1) (1,3)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                var j = 2;
                foreach (var payroll in CalculationOfWages())
                {
                    worksheet.Cells["A" + j].Value = payroll.Id;
                    worksheet.Cells["B" + j].Value = payroll.EmployeeName;
                    worksheet.Cells["C" + j].Value = payroll.Salary.ToString("$#,0.00;($#,0.00)");

                    j++;
                }

                package.Save();
            }

            return URL;
        }

        public void AddArticles(ExcelWorksheet excelWorksheet)
        {
            //Col index
            int noIndex = 1;
            int titleIndex = 2;
            int personInChargeIndex = 3;

            //Get row start if it name "No".
            int startRowIndex = GetRowStart(excelWorksheet);

            //Get row end if it name "" or can't convert to int().
            int endRowIndex = GetRowEnd(startRowIndex, excelWorksheet);

            
            for (int i = startRowIndex; i < endRowIndex; i++)
            {
                var article = new Article();
                string colNoValue = excelWorksheet.Cells[i, noIndex].Text;
                if (!string.IsNullOrEmpty(colNoValue))
                {
                    int id = 0;
                    int.TryParse(excelWorksheet.Cells[i, noIndex].Text, out id);
                    article.Id = id;
                    article.Title = excelWorksheet.Cells[i, titleIndex].Text;
                    article.PersonInCharge = excelWorksheet.Cells[i, personInChargeIndex].Text;
                }

                articles.Add(article);

            }
        }

        //Get row start if it name "No".
        private int GetRowStart(ExcelWorksheet excelWorksheet)
        {
            int startRow = excelWorksheet.Dimension.Start.Row;
            int endRow = excelWorksheet.Dimension.End.Row;
            int noColIndex = 1;
            int rowStart = 1;
            string startWith = "No";

            for (int i = startRow; i <= endRow; i++)
            {
                if (excelWorksheet.Cells[i, noColIndex].Text.ToLower().Contains(startWith.ToLower()))
                {
                    rowStart = i + 1;
                    break;
                }
            }

            return rowStart;
        }

        //Get row end if No col is null.
        private int GetRowEnd(int startRowIndex, ExcelWorksheet excelWorksheet)
        {
            int endRow = excelWorksheet.Dimension.End.Row;
            int noColIndex = 1;
            int rowEnd = 0;
            string endWith = "";

            for (int i = startRowIndex; i <= endRow+1; i++)
            {
                if (excelWorksheet.Cells[i, noColIndex].Text == endWith)
                {
                    rowEnd = i;
                    break;
                }
            }

            return rowEnd;
        }

    }
}